*[AppImage]: An AppImage is a portable/self-contained application. It contains all of the dependencies required to run the respective program.
*[Flatpak]: A flatpak is a sandboxed application, flatpaks run in an isolated environment which allow for granular control over permissions. On a Steam Deck, flatpaks can be installed, managed, and updated through Discover. 
