
# The Cheat Sheet is Your Tool to Success.

***

## Cheat Sheet Table of Contents


[TOC]

## Cheat Sheets
[Back to the Top](#cheat-sheet-table-of-contents)

***

### Arcade and MAME Related Emulation Cheat Sheet
[Back to the Top](#cheat-sheet-table-of-contents)

***

{{ read_csv('arcade-and-mame-related-emulation-cheat-sheet.csv') }}

* #### Final Burn Neo
* #### MAME 2003
* #### MAME 2010
* #### MAME Current
* #### MAME (Standalone)
* #### Philips CD-i
* #### Tiger Electronics Game.com
* #### VTech V.Smile


***

### Atari Cheat Sheet
[Back to the Top](#cheat-sheet-table-of-contents)


{{ read_csv('atari-cheat-sheet.csv') }}



***

### Bandai Cheat Sheet

{{ read_csv('bandai-cheat-sheet.csv') }}

***

### Game Engine Recreations Cheat Sheet
[Back to the Top](#cheat-sheet-table-of-contents)

{{ read_csv('game-engine-recreations-cheat-sheet.csv') }}


* #### DooM
* #### EasyRPG
* #### Pico-8
* #### ScummVM

***

### Mattel Cheat Sheet
[Back to the Top](#cheat-sheet-table-of-contents)

{{ read_csv('mattel-cheat-sheet.csv') }}



* #### Mattel Electronics Intellivison

***

### Microsoft Cheat Sheet
[Back to the Top](#cheat-sheet-table-of-contents)


{{ read_csv('microsoft-cheat-sheet.csv') }}



* #### Microsoft Xbox
* #### Microsoft Xbox 360
* #### Microsoft MSX 1
* #### Microsoft MSX 2

***

### NEC Cheat Sheet
[Back to the Top](#cheat-sheet-table-of-contents)

{{ read_csv('nec-cheat-sheet.csv') }}



* #### NEC TurboGrafx-16 / PC Engine 
* #### TurboGrafx-16 / PC Engine CD
* #### NEC SuperGrafx

***

### Nintendo Cheat Sheet
[Back to the Top](#cheat-sheet-table-of-contents)



{{ read_csv('nintendo-cheat-sheet.csv') }}




***

### Panasonic Cheat Sheet
[Back to the Top](#cheat-sheet-table-of-contents)

{{ read_csv('panasonic-cheat-sheet.csv') }}


* #### Panasonic 3DO

***

### Personal Computers Cheat Sheet
[Back to the Top](#cheat-sheet-table-of-contents)


{{ read_csv('personal-computers-cheat-sheet.csv') }}



***

### Sega Cheat Sheet
[Back to the Top](#cheat-sheet-table-of-contents)


{{ read_csv('sega-cheat-sheet.csv') }}


***

### SNK Cheat Sheet
[Back to the Top](#cheat-sheet-table-of-contents)

{{ read_csv('snk-cheat-sheet.csv') }}


* #### SNK Neo Geo CD
* #### SNK Neo Geo Pocket
* #### SNK Neo Geo Pocket Color

***

### Sony Cheat Sheet
[Back to the Top](#cheat-sheet-table-of-contents)


{{ read_csv('sony-cheat-sheet.csv') }}




*** 
