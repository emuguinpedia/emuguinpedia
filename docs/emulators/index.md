# Emulators

<div class="grid cards _bz" markdown>


- :material-controller-classic: **Arcade Emulators**{ style="font-size: 1.1rem" }

  ***

  - [Hypseus Singe](./arcade/hypseus-singe.md)
  - [MAME](./arcade/mame.md)
  - [Model 2 Emulator](./arcade/model-2-emulator.md)
  - [Supermodel](./arcade/supermodel.md)

- :material-silverware-fork: **Emulator Forks**{ style="font-size: 1.1rem" }

  ***

  - [Mario Party Netplay (Dolphin Fork)](./emulator-forks/mario-party-netplay.md)
  - [Melon Mix (melonDS Fork)](./emulator-forks/melon-mix.md)
  - [melonPrimeDS (melonDS Fork)](./emulator-forks/melon-primeds.md)
  - [PrimeHack (Dolphin Fork)](./emulator-forks/primehack.md)
  - [Project Rio (Dolphin Fork)](./emulator-forks/project-rio.md)
  - [Triforce (Dolphin Fork)](./emulator-forks/triforce.md)

- :material-microsoft-xbox: **Microsoft Emulators**{ style="font-size: 1.1rem" }

  ***

  - [Cxbx-Reloaded](./microsoft/cxbx-reloaded.md)
  - [Xemu](./microsoft/xemu.md)
  - [Xenia](./microsoft/xenia.md)

- :material-gamepad-round: **Miscellaneous**{ style="font-size: 1.1rem" }

  ***

  - [DREAMM](./miscellaneous/dreamm.md)
  - [EasyRPG](./miscellaneous/easyrpg.md)
  - [GZDoom](./miscellaneous/gzdoom.md)
  - [Pico-8](./miscellaneous/pico-8.md)
  - [ScummVM](./miscellaneous/scummvm.md)
  - [Visual Pinball](./miscellaneous/visual-pinball.md)

- :material-cellphone: **Mobile Emulators**{ style="font-size: 1.1rem" }

  ***

  - [EKA2L1](./mobile/eka2l1.md)
  - [FreeJ2ME](./mobile/freej2me.md)
  - [touchHLE](./mobile/touchhle.md)
  - [Waydroid](./mobile/waydroid.md)

- :material-controller: **Multi-System Emulators**{ style="font-size: 1.1rem" }

  ***

  - [ares](./multi-system/ares.md)
  - [Mednafen](./multi-system/mednafen.md)
  - [Mesen](./multi-system/mesen.md)
  - [RetroArch](./multi-system/retroarch.md)

- :material-nintendo-game-boy: **Nintendo Emulators**{ style="font-size: 1.1rem" }

  ***

  - [Cemu](./nintendo/cemu.md)
  - [Citra](./nintendo/citra.md/)
  - [Dolphin](./nintendo/dolphin.md)
  - [melonDS](./nintendo/melonds.md)
  - [mGBA](./nintendo/mgba.md/)
  - [Rosalie's Mupen GUI](./nintendo/rmg.md)
  - [Ryujinx](./nintendo/ryujinx.md)

- :material-desktop-classic: **Personal Computers**{ style="font-size: 1.1rem" }

  ***

  - [DOSBox-X](./personal-computers/dosbox-x.md)
  - [FS-UAE](./personal-computers/fs-uae.md)
  - [Neko Project II](./personal-computers/neko-project-ii.md)
  - [Tsugaru](./personal-computers/tsugaru.md)

- :material-gamepad-variant: **Sega Emulators**{ style="font-size: 1.1rem" }

  ***
  
  - [BlastEm](./sega/blastem.md)
  - [Flycast](./sega/flycast.md)
  - [Kronos](./sega/kronos.md)
  - [redream](./sega/redream.md)

- :material-sony-playstation: **Sony Emulators**{ style="font-size: 1.1rem" }

  ***

  - [DuckStation](./sony/duckstation.md)
  - [PCSX2](./sony/pcsx2.md)
  - [Play!](./sony/play.md)
  - [RPCS3](./sony/rpcs3.md)
  - [ShadPS4](./sony/shadps4.md)