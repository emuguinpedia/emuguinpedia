# Welcome to Emuguinpedia!

[Wiki Link](https://emuguinpedia.com/)

## What is Emuguinpedia? 

Emuguinpedia is meant to be a catch-all wiki for emulation on Linux with an eye for the Steam Deck and immutable operating systems. This wiki will cover how to install and configure various emulators using universal Linux packaging such as AppImages and Flatpaks. When possible, pages will adhere to the developer's preferred packaging format. When a packaging format does not exist, a page will include a disclaimer and helpful tips when asking for support. 

If an emulator only exists on Windows, this wiki will cover instructions on how to install the emulator through Wine/Proton with heavy emphasis that support for these emulators is not guaranteed when asking for help in official channels. 

The goals of the wiki are as follows:

* To cover basic emulation information such as expected BIOS and ROM file types.
* To teach a user how to install and configure an emulator for the first time.
* To teach a user how to navigate to an emulator's configuration folders on Linux with instructions geared towards people who have never used a Linux distro before.
* To teach a user how to interact with configuration files
* To teach a user how to interact with a terminal.
* To teach a user how to augment an emulator's features (example: enable gyroscope).
* To extend the documentation available for emulator developers. 

## Contributions

Emuguinpedia is always looking for contributors to add new wiki pages and enhance the documentation. This wiki is hosted on a public Gitlab page, pull requests welcome. 

## Steam Deck and Game Mode

Many of the sections on this wiki will cover "Game Mode" specific steps. These steps will only be applicable if you are using a Steam Deck, a distro with "Game Mode" support (Bazzite, Nobara, Manjaro, etc.), or if you have installed "Game Mode" on your own (through the Gamescope package, [https://github.com/ValveSoftware/gamescope](https://github.com/ValveSoftware/gamescope)).

## Front-Ends

This wiki will cover how to configure and set up front-ends which can be used to launch and manage your ROMs. Sections may include specific steps on how to configure an emulator for a front-end. 

If you have created a front-end or would like to see a front-end page, pull requests are welcome to cover additional front-ends on the wiki. 

## Disclaimer

This wiki does not seek to be a replacement for official emulation documentation but rather fill in the gaps. When possible, pages will link to official documentation and encourage the use of the original documentation when possible.

Emulator developers are more than welcome to take the work here and use it in their own documentation. 

***